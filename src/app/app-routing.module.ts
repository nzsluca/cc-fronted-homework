import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormComponent } from './form/form.component';
import { ImportantComponent } from './important/important.component';
import { QuestionmarkComponent } from './questionmark/questionmark.component';

const routes: Routes = [
  {
    path:'form',component:FormComponent
  },
  {
    path:'important',component:ImportantComponent
  },
  {
    path:'??',component:QuestionmarkComponent
  },
  {
    path: '',   redirectTo: '/form', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
