import { Component, OnInit } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],

})
export class FormComponent implements OnInit {
  constructor(private _snackBar: MatSnackBar) {}

  showSpinner = false;

  loadData(){
    this.showSpinner = true;
    setTimeout(() =>{
      this.showSpinner = false
    }, 4000)

  }

  openSnackBar(message, action) {
    setTimeout(() => {
      this._snackBar.open(message,action);
    }, 4000)

  }

  all(message, action){
    this.loadData();
    this.openSnackBar(message, action);

  }


  ngOnInit(): void {
  }

}

